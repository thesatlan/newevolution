#pragma once

#include <cassert>

class Vector2Test
{
private:
	void testConstructor();
	void testCopyConstructor();
	void testAssignmentOperator();

	void testGetOperator();
	void testSetOperator();

	void testEqualOperator();
	void testNotEqualOperator();

	void testNegOperator();

	void testAddEqualOperator();
	void testMinusEqualOperator();
	void testMulEqualOperator();
	void testDivEqualOperator();

	void testPlusOperator();
	void testMinusOperator();
	void testMulNumOperator();
	void testDivOperator();

	void testMulVecOperator();

	void testGetValues();
	void testGetSize();
	void testGetDirectionalSize();

	void testRotate();
	void testNormalize();

	void testMisc();

public:
	Vector2Test();
	~Vector2Test();
};


Vector2Test::Vector2Test()
{
	testConstructor();
	testCopyConstructor();
	testAssignmentOperator();
	testGetOperator();
	testSetOperator();
	testEqualOperator();
	testNotEqualOperator();
	testNegOperator();
	testAddEqualOperator();
	testMinusEqualOperator();
	testMulEqualOperator();
	testDivEqualOperator();
	testPlusOperator();
	testMinusOperator();
	testMulNumOperator();
	testDivOperator();
	testMulVecOperator();
	testGetValues();
	testGetSize();
	testGetDirectionalSize();
	testRotate();
	testNormalize();
	testMisc();
}

Vector2Test::~Vector2Test()
{}

void Vector2Test::testConstructor()
{
	Vector2 vec12(1, 2);
	Vector2 vec1011(1.0, 1.1);

	assert(vec12[0] == 1);
	assert(vec12[1] == 2);
	assert(vec1011[0] == 1.0);
	assert(vec1011[1] == 1.1);
}

void Vector2Test::testCopyConstructor()
{
	Vector2 vec1(1, 2);
	Vector2 vec2 = vec1;
	Vector2 vec3(vec1);

	assert(vec1 == vec2);
	assert(vec1 == vec3);
	assert(vec2 == vec3);
}

void Vector2Test::testAssignmentOperator()
{
	Vector2 vec1(1, 1);
	Vector2 vec2(2, 2);

	vec1 = vec1;
	vec2 = vec1;

	assert(vec1 == vec2);
	assert(vec1 == vec1);
}

void Vector2Test::testGetOperator()
{
	Vector2 vec(50, 22);

	assert(vec[0] == 50);
	assert(vec[1] == 22);
}

void Vector2Test::testSetOperator()
{
	Vector2 vec(1, 2);
	vec[0] = 50;
	vec[1] = 22;

	assert(vec == Vector2(50,22));
}

void Vector2Test::testEqualOperator()
{
	Vector2 vec1(1, 1);
	Vector2 vec2(1, 1);
	Vector2 vec3(1, 2);

	assert(vec1 == vec2);
	assert(!(vec1 == vec3));
	assert(!(vec2 == vec3));
}

void Vector2Test::testNotEqualOperator()
{
	Vector2 vec1(1, 1);
	Vector2 vec2(1, 1);
	Vector2 vec3(1, 2);

	assert(!(vec1 != vec2));
	assert(vec1 != vec3);
	assert(vec2 != vec3);
}

void Vector2Test::testNegOperator()
{
	Vector2 vec1(1, 2);

	assert(-vec1 == Vector2(-1, -2));
}

void Vector2Test::testAddEqualOperator()
{
	Vector2 vec(1, 2);
	vec += Vector2(20, 20);

	assert(vec == Vector2(21, 22));
}

void Vector2Test::testMinusEqualOperator()
{
	Vector2 vec(1, 2);
	vec -= Vector2(20, 20);

	assert(vec == Vector2(-19, -18));
}

void Vector2Test::testMulEqualOperator()
{
	Vector2 vec(1, 2);
	vec *= 5;

	assert(vec == Vector2(5, 10));
}

void Vector2Test::testDivEqualOperator()
{
	Vector2 vec(10, 20);
	vec /= 5;

	assert(vec == Vector2(2, 4));
}

void Vector2Test::testPlusOperator()
{
	assert(Vector2(1, 3) + Vector2(3, 1) == Vector2(4, 4));
}

void Vector2Test::testMinusOperator()
{
	assert(Vector2(1, 3) - Vector2(3, 1) == Vector2(-2, 2));
}

void Vector2Test::testMulNumOperator()
{
	assert(Vector2(1, 3) * 3 == Vector2(3, 9));
}

void Vector2Test::testDivOperator()
{
	assert(Vector2(3, 9) / 3 == Vector2(1, 3));
}

void Vector2Test::testMulVecOperator()
{
	assert(Vector2(1, 3) * Vector2(3, 1) == 6);
}

void Vector2Test::testGetValues()
{
	Vector2 vec(1, 3);
	const double * values = vec.getValues();
	assert(values[0] == 1);
	assert(values[1] == 3);
}

void Vector2Test::testGetSize()
{
	Vector2 vec1(0, 1);
	Vector2 vec2(3, 4);
	Vector2 vec3(200, 0);
	Vector2 vec4(-3, -4);

	assert(Vector2(0, 1).getSize() == 1);
	assert(Vector2(3, 4).getSize() == 5);
	assert(Vector2(200, 0).getSize() == 200);
	assert(Vector2(-3, -4).getSize() == 5);
}

void Vector2Test::testGetDirectionalSize()
{
	assert(Vector2(0, 1).getDirectionalSize(Vector2(0, 1)) == 1);
	assert(Vector2(0, 1).getDirectionalSize(Vector2(1, 0)) == 0);
	assert(Vector2(15, 17).getDirectionalSize(Vector2(0, 1)) == 17);
	assert(Vector2(15, 17).getDirectionalSize(Vector2(1, 0)) == 15);
	assert(Vector2(15, 17).getDirectionalSize(Vector2(100, 0)) == 15);
	assert(Vector2(3, 4).getDirectionalSize(Vector2(-4, 3)) == 0);
	assert(Vector2(3, 4).rotate(20).getDirectionalSize(Vector2(1, 0).rotate(20)) == 3);
}

void Vector2Test::testRotate()
{
	assert(Vector2(1, 0).rotate(360) == Vector2(1, -0));
	assert(Vector2(1, 0).rotate(90) == Vector2(0, 1));
	assert(Vector2(1, 0).rotate(180) == Vector2(-1, 0));
	assert(Vector2(3, 4).rotate(90) == Vector2(-4, 3));
	assert(Vector2(3, 4).rotate(30) == Vector2(-4, 3).rotate(-60));
}

void Vector2Test::testNormalize()
{
	assert(Vector2(5, 0).normalize() == Vector2(1, 0));
	assert(Vector2(0, 7).normalize() == Vector2(0, 1));
	assert(Vector2(1, 1).normalize() == Vector2(5, 5).normalize());
}

void Vector2Test::testMisc()
{
	assert(Vector2(5, 0).rotate(20).normalize().rotate(70) == Vector2(0, 1));
}
