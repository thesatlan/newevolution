#pragma once

#include "Vector2.cuh"
#include "Joint.cuh"
#include "Muscle.cuh"

#define EARTH_G		(9.807)
#define MIN_JOINTS	(3)
#define MAX_JOINTS	(5)

class Creature
{
private:
	const unsigned int joints_num;
	Joint* joints;
	const unsigned int muscles_num;
	Muscle* muscles;

	unsigned int cur_muscles_num; // This member is for creating the random creature.
	Vector2 center_of_mass; // This member contains the center of mass every time after calculation.

	__host__ __device__ unsigned int max_muscles(int num_of_joints);
	__host__ __device__ bool are_joints_connected(unsigned int joint_1_idx, unsigned int joint_2_idx);

	__host__ Creature(
		unsigned int joints_num,
		Joint* joints,
		unsigned int muscles_num,
		Muscle* muscles,
		double mutation_factor);

public:
	__host__ Creature();
	__host__ Creature(const Creature &other);
	__host__ ~Creature();
	__host__ Creature mutate(double factor);

	__host__ __device__ unsigned int getJointsNum();
	__host__ __device__ const Joint* getJoints() const;
	__host__ __device__ unsigned int getMusclesNum();
	__host__ __device__ const Muscle* getMuscles() const;

	__host__ __device__ void calculateCenterOfMass();
	__host__ Vector2 getCenterOfMass() const;
	__host__ double score() const;
	__host__ bool operator<(const Creature& other) const;

	__host__ __device__ void step(const double time_interval, const Environment* environment);
	__host__ void reset();
};


Creature::Creature():
	joints_num(MIN_JOINTS + rand()%(1 + MAX_JOINTS - MIN_JOINTS)),
	center_of_mass(Vector2(0, 0)),
	cur_muscles_num(0),
	muscles_num(joints_num + rand()%(1 + max_muscles(joints_num) - joints_num))
{
	// Allocating space for the joints and muscles.
	cudaError_t err1 = cudaMallocManaged((void**)&joints, joints_num * sizeof(Joint));
	cudaError_t err2 = cudaMallocManaged((void**)&muscles, muscles_num * sizeof(Muscle));

	if (err1 != cudaSuccess || err2 != cudaSuccess)
	{
		printf("cudaMallocManaged failed!\n");
		return;
	}

	for (unsigned int i = 0; i < joints_num; i++)
	{
		// Creating the joint.
		new (&joints[i]) Joint();
		
		// Connecting the joint to a random joint via a muscle.
		// It is ok to do that even if the next joint doesn't exist 
		//   yet because we only give a pointer to a known location.
		unsigned int connected_joint_idx = rand() % joints_num;
		if (!are_joints_connected(i, connected_joint_idx))
		{
			new (&muscles[cur_muscles_num]) Muscle(i, connected_joint_idx);
			cur_muscles_num++;
		}
		// It is OK if the joints are already connected.
		// In this case we just skip this connection.
	}

	for (; cur_muscles_num < muscles_num; cur_muscles_num++)
	{
		// Finding random 2 not connected joints.
		unsigned int rand_joint_1_idx = rand() % joints_num;
		unsigned int rand_joint_2_idx = rand() % joints_num;
		while (are_joints_connected(rand_joint_1_idx, rand_joint_2_idx))
		{
			rand_joint_1_idx = rand() % joints_num;
			rand_joint_2_idx = rand() % joints_num;
		}
		new (&muscles[cur_muscles_num]) Muscle(rand_joint_1_idx, rand_joint_2_idx);
	}
}

Creature::Creature(const Creature &other):
	joints_num(other.joints_num),
	center_of_mass(Vector2(0, 0)),
	muscles_num(other.muscles_num)
{
	// Allocating space for the joints and muscles.
	cudaError_t err1 = cudaMallocManaged((void**)&joints, joints_num * sizeof(Joint));
	cudaError_t err2 = cudaMallocManaged((void**)&muscles, muscles_num * sizeof(Muscle));

	if (err1 != cudaSuccess || err2 != cudaSuccess)
	{
		printf("cudaMallocManaged failed!\n");
		return;
	}

	cudaMemcpy(joints, other.joints, joints_num * sizeof(Joint), cudaMemcpyDefault);
	cudaMemcpy(muscles, other.muscles, muscles_num * sizeof(Muscle), cudaMemcpyDefault);
}

Creature::Creature(
	unsigned int joints_num,
	Joint* other_joints,
	unsigned int muscles_num,
	Muscle* other_muscles,
	double mutation_factor):
	joints_num(joints_num),
	center_of_mass(Vector2(0, 0)),
	cur_muscles_num(0),
	muscles_num(muscles_num)
{
	// Allocating space for the joints and muscles.
	cudaError_t err1 = cudaMallocManaged((void**)&joints, joints_num * sizeof(Joint));
	cudaError_t err2 = cudaMallocManaged((void**)&muscles, muscles_num * sizeof(Muscle));

	if (err1 != cudaSuccess || err2 != cudaSuccess)
	{
		printf("cudaMallocManaged failed!\n");
		return;
	}

	for (unsigned int i = 0; i < joints_num; i++)
	{
		new (&joints[i]) Joint(other_joints[i].mutate(mutation_factor));
	}
	for (unsigned int i = 0; i < muscles_num; i++)
	{
		new (&muscles[i]) Muscle(other_muscles[i].mutate(mutation_factor));
	}
}

Creature::~Creature()
{
	for (unsigned int i = 0; i < joints_num; i++)
	{
		joints[i].~Joint();
	}
	cudaFree(joints);

	for (unsigned int i = 0; i < muscles_num; i++)
	{
		muscles[i].~Muscle();
	}
	cudaFree(muscles);
}

Creature Creature::mutate(double factor)
{
	return Creature(
		this->joints_num,
		this->joints,
		this->muscles_num,
		this->muscles,
		factor
	);
}

unsigned int Creature::max_muscles(int num_of_joints)
{
	int sum = 0;
	for (int i = 1; i < num_of_joints; i++)
	{
		sum += i;
	}
	return sum;
}

bool Creature::are_joints_connected(unsigned int joint_1_idx, unsigned int joint_2_idx)
{
	if (joint_1_idx == joint_2_idx)
	{
		return true;
	}

	for (unsigned int i = 0; i < cur_muscles_num; i++)
	{
		if ((muscles[i].getJoint1Idx() == joint_1_idx) &&
			(muscles[i].getJoint2Idx() == joint_2_idx))
		{
			return true;
		}
		if ((muscles[i].getJoint1Idx() == joint_2_idx) &&
			(muscles[i].getJoint2Idx() == joint_1_idx))
		{
			return true;
		}
	}

	return false;
}

unsigned int Creature::getJointsNum()
{
	return joints_num;
}

const Joint* Creature::getJoints() const
{
	return joints;
}

unsigned int Creature::getMusclesNum()
{
	return muscles_num;
}

const Muscle* Creature::getMuscles() const
{
	return muscles;
}

Vector2 Creature::getCenterOfMass() const
{
	return this->center_of_mass;
}

void Creature::calculateCenterOfMass()
{
	double mass_sum = 0;
	double x_center = 0;
	double y_center = 0;

	for (unsigned int i = 0; i < joints_num; i++)
	{
		double mass = joints[i].getMass();
		Vector2 location = joints[i].getLocation();

		mass_sum += mass;
		x_center += mass * location[0];
		y_center += mass * location[1];
	}

	x_center /= mass_sum;
	y_center /= mass_sum;

	this->center_of_mass = Vector2(x_center, y_center);
}

double Creature::score() const
{
	return this->getCenterOfMass()[0];
}

bool Creature::operator<(const Creature& other) const
{
	return this->score() < other.score();
}

__global__ void step_joints(const double time_interval, const Environment* environment, Joint* joints, unsigned int joints_num)
{
	int i = blockIdx.x * blockDim.x + threadIdx.x;

	if (i < joints_num)
	{
		Vector2 gravity_force(0, -joints[i].getMass() * (EARTH_G));
		joints[i].addForce(gravity_force);

		joints[i].step(time_interval, environment);
	}
}

void Creature::step(const double time_interval, const Environment* environment)
{
	// Adding muscles forces to joints.
	for (unsigned int i = 0; i < muscles_num; ++i)
	{
		muscles[i].applyCollisionIfExists(joints);
		muscles[i].addMuscleForceToJoints(time_interval, joints);
	}

	// Adding gravity force to the joints and moving the joints.
	for (unsigned int i = 0; i < joints_num; ++i)
	{
		Vector2 gravity_force(0, -joints[i].getMass() * (EARTH_G));
		joints[i].addForce(gravity_force);

		joints[i].step(time_interval, environment);
	}
}

void Creature::reset()
{
	for (unsigned int i = 0; i < joints_num; i++)
	{
		joints[i].reset();
	}
	for (unsigned int i = 0; i < muscles_num; i++)
	{
		muscles[i].reset();
	}

	calculateCenterOfMass();
	for (unsigned int i = 0; i < joints_num; i++)
	{
		joints[i].move(Vector2(-center_of_mass[0], 0));
	}
}
