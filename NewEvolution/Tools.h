#pragma once

#include <stdlib.h>

__host__ double insert_to_margins(double value, double min_margin, double max_margin)
{
	if (value < min_margin)
	{
		return min_margin;
	}
	if (value > max_margin)
	{
		return max_margin;
	}
	return value;
}

__host__ double mutate_value(
	double orig_value,
	double min_margin,
	double max_margin,
	double mutation_factor)
{
	double mutation = orig_value + (mutation_factor * (max_margin - min_margin) * (rand() % 1024) / 1024);
	return insert_to_margins(mutation, min_margin, max_margin);
}

__host__ double rand_range(double min, double max, bool exclude_min = false)
{
	unsigned int rand_val = rand() % 128;
	if (exclude_min)
	{
		rand_val += 1;
	}
	return min + rand_val * ((max - min) / 128);
}