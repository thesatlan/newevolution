#pragma once

#include <stdlib.h>
#include "Vector2.cuh"
#include "Environment.cuh"
#include "Tools.h"

#define MIN_MASS		(0.01)
#define MIN_FRICTION	(0.01)
#define MIN_LOCATION	(0)

#define MAX_MASS		(0.5)	// kg
#define MAX_FRICTION	(1)	    // u
#define MAX_LOCATION	(0.5)	// m

#define MASS_MUTATION_FACTOR		(0.05)
#define FRICTION_MUTATION_FACTOR	(0.05)
#define LOCATION_MUTATION_FACTOR	(0.025)

class Joint
{
private:
	const double mass;
	const double friction_const;
	const Vector2 init_location;

	Vector2 force;
	Vector2 speed;
	Vector2 location;

	__host__ __device__ void applySpeed(const double time_interval, const Environment* environment);
	__host__ __device__ void applyForce(const double time_interval);

public:
	__host__ Joint();
	__host__ Joint(
		const double mass, 
		const double friction_const, 
		const Vector2 & init_location);
	__host__ Joint mutate(double factor);

	__host__ __device__ double getMass() const;
	__host__ __device__ double getFrictionConst() const;
	__host__ __device__ const Vector2& getLocation() const;
	__host__ __device__ const Vector2& getSpeed() const;
	__host__ __device__ void setSpeedInDirection(const Vector2 & direction, const double new_size);
	__host__ void move(const Vector2 & direction);

	__host__ __device__ void addForce(const Vector2 & added_force);

	__host__ __device__ void step(const double time_interval, const Environment* environment);
	__host__ void reset();
};


Joint::Joint() :
	mass(rand_range(MIN_MASS, MAX_MASS)),
	friction_const(rand_range(MIN_FRICTION, MAX_FRICTION)),
	init_location(Vector2(rand_range(MIN_LOCATION, MAX_LOCATION), rand_range(MIN_LOCATION, MAX_LOCATION))),
	force(Vector2(0, 0)),
	speed(Vector2(0, 0)),
	location(init_location)
{

}

Joint::Joint(const double mass, const double friction_const, const Vector2 & init_location) :
	mass(mass), //mass
	friction_const(friction_const),
	init_location(init_location),
	force(Vector2(0, 0)),
	speed(Vector2(0, 0)),
	location(init_location)
{

}

Joint Joint::mutate(double factor)
{
	// mutating only one of the values (50% chance of mutating nothing)
	unsigned int rand_val = rand() % 6;
	return Joint(
		mutate_value(this->mass, MIN_MASS, MAX_MASS, (rand_val == 0) * factor),
		mutate_value(this->friction_const, MIN_FRICTION, MAX_FRICTION, (rand_val == 1) * factor),
		Vector2(
			mutate_value(this->init_location[0], MIN_LOCATION, MAX_LOCATION, (rand_val == 2) * factor),
			mutate_value(this->init_location[1], MIN_LOCATION, MAX_LOCATION, (rand_val == 2) * factor))
	);
}

double Joint::getMass() const
{
	return this->mass;
}

double Joint::getFrictionConst() const
{
	return this->friction_const;
}

const Vector2& Joint::getLocation() const
{
	return this->location;
}

const Vector2& Joint::getSpeed() const
{
	return this->speed;
}

void Joint::applySpeed(const double time_interval, const Environment* environment)
{
	this->location += this->speed * time_interval;
	environment->fixLocationAndSpeed(this->location, this->speed);
}

void Joint::applyForce(const double time_interval)
{
	this->speed += force * time_interval / mass;
	this->force = Vector2(0, 0);
}

void Joint::addForce(const Vector2 & added_force)
{
	this->force += added_force;
}

void Joint::setSpeedInDirection(const Vector2 & direction, const double new_size)
{
	this->speed += direction.normalize() * (new_size - this->speed.getDirectionalSize(direction));
}

void Joint::move(const Vector2 & direction)
{
	this->location += direction;
}

void Joint::step(const double time_interval, const Environment* environment)
{
	Vector2 normal_force = environment->getNormalForce(this->location, this->force);
	this->addForce(normal_force);

	if (normal_force != Vector2(0, 0))
	{
		Vector2 friction_force(0, 0);
		Vector2 left_normal = normal_force.rotate(90);
		Vector2 right_normal = -left_normal;

		// Checking the speed direction. against that direction the friction will apply.
		if (this->speed.getDirectionalSize(left_normal) > 0)
		{
			friction_force = right_normal * friction_const;
		}
		else if (this->speed.getDirectionalSize(right_normal) > 0)
		{
			friction_force = left_normal * friction_const;
		}
		this->addForce(friction_force);
	}

	// "/2" because the speed changes by it all at once.
	this->applyForce(time_interval / 2);
	applySpeed(time_interval, environment);
}

void Joint::reset()
{
	this->force = Vector2(0, 0);
	this->speed = Vector2(0, 0);
	this->location = this->init_location;
}
