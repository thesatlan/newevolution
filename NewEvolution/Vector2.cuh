#pragma once

#define EPSILON (0.000000000001)
#define PI	(3.14159265358979323846264338327950288419716939937510)

#define HEIGHT (1)

class Vector2
{
private:
	double values[2];

public:
	__host__ __device__ Vector2(const double x, const double y);
	__host__ __device__ Vector2(const Vector2 &other);
	__host__ __device__ const Vector2 operator=(const Vector2 &other);
	__host__ __device__ ~Vector2();

	__host__ __device__ const double & operator[](const unsigned int index) const;
	__host__ __device__ double & operator[](const unsigned int index);

	__host__ __device__ bool operator==(const Vector2 & other) const;
	__host__ __device__ bool operator!=(const Vector2 & other) const;

	__host__ __device__ Vector2 operator-() const;

	__host__ __device__ void operator+=(const Vector2 &other);
	__host__ __device__ void operator-=(const Vector2 &other);
	__host__ __device__ void operator*=(const double num);
	__host__ __device__ void operator/=(const double num);

	__host__ __device__ Vector2 operator+(const Vector2 &other) const;
	__host__ __device__ Vector2 operator-(const Vector2 &other) const;
	__host__ __device__ Vector2 operator*(const double num) const;
	__host__ __device__ Vector2 operator/(const double num) const;

	__host__ __device__ double operator*(const Vector2 &other) const;

	__host__ __device__ const double* getValues() const;

	__host__ __device__ double getSize() const;
	__host__ __device__ double getDirectionalSize(const Vector2 &direction) const;
	__host__ __device__ void setDirectionalSize(const Vector2 &direction, double new_size);
	__host__ __device__ 
	__host__ __device__ const Vector2 rotate(const double angle) const;
	__host__ __device__ const Vector2 normalize() const;
};

int five()
{
	return 5;
}

Vector2::Vector2(const double x, const double y) :
	values{x,y}
{
}

Vector2::Vector2(const Vector2 & other) :
	values{other[0], other[1]}
{
}

const Vector2 Vector2::operator=(const Vector2 & other)
{
	if (this == &other)
	{
		return (*this);
	}

	this->values[0] = other[0];
	this->values[1] = other[1];

	return (*this);
}

Vector2::~Vector2()
{
}

const double & Vector2::operator[](const unsigned int index) const
{
	return this->values[index];
}

double & Vector2::operator[](const unsigned int index)
{
	return this->values[index];
}

bool Vector2::operator==(const Vector2 & other) const
{
	return (abs(this->values[0] - other[0]) < EPSILON) && (abs(this->values[1] - other[1]) < EPSILON);
}

bool Vector2::operator!=(const Vector2 & other) const
{
	return !((*this) == other);
}

Vector2 Vector2::operator-() const
{
	Vector2 vec(-this->values[0], -this->values[1]);

	if (isnan(vec[0]))
	{
		return vec;
	}
	return vec;
}

void Vector2::operator+=(const Vector2 & other)
{
	this->values[0] += other[0];
	this->values[1] += other[1];
}

void Vector2::operator-=(const Vector2 & other)
{
	this->values[0] -= other[0];
	this->values[1] -= other[1];
}

void Vector2::operator*=(const double num)
{
	this->values[0] *= num;
	this->values[1] *= num;
}

void Vector2::operator/=(const double num)
{
	this->values[0] /= num;
	this->values[1] /= num;
}

Vector2 Vector2::operator+(const Vector2 & other) const
{
	Vector2 vec = Vector2(*this);
	vec += other;
	return vec;
}

Vector2 Vector2::operator-(const Vector2 & other) const
{
	Vector2 vec = Vector2(*this);
	vec -= other;
	return vec;
}

Vector2 Vector2::operator*(const double num) const
{
	Vector2 vec = Vector2(*this);
	vec *= num;
	return vec;
}

Vector2 Vector2::operator/(const double num) const
{
	Vector2 vec = Vector2(*this);
	vec /= num;
	return vec;
}

double Vector2::operator*(const Vector2 & other) const
{
	return this->values[0] * other[0] + this->values[1] * other[1];
}

const double * Vector2::getValues() const
{
	return this->values;
}

double Vector2::getSize() const
{
	return sqrt((*this) * (*this));
}

double Vector2::getDirectionalSize(const Vector2 &direction) const
{
	const double direction_size = direction.getSize();

	return ((*this) * direction) / direction_size; // Might throw division by 0 error if you are stupid...
}

void Vector2::setDirectionalSize(const Vector2 &direction, double new_size)
{
	(*this) += direction.normalize() * (new_size - this->getDirectionalSize(direction));
}

const Vector2 Vector2::rotate(const double angle) const
{
	Vector2 copy = *this;
	const double radians = angle * PI / 180;
	const double first_val = copy[0];
	const double second_val = copy[1];

	copy[0] = cos(radians) * first_val - sin(radians) * second_val;
	copy[1] = sin(radians) * first_val + cos(radians) * second_val;

	return copy;
}

const Vector2 Vector2::normalize() const
{
	if (this->getSize() == 0)
	{
		return Vector2(1, 0);
	}

	return (*this) / (this->getSize());
}
