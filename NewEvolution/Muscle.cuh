#pragma once

#include "Vector2.cuh"
#include "Joint.cuh"
#include "Tools.h"

#define MIN(a, b) (((a) < (b)) ? (a) : (b))

#define CONTRACTING (0)
#define	EXTENDING (1)

#define MIN_CONTRACTED_LENGTH				(0.1)
#define MIN_EXTENDED_LENGTH					(0.1)
#define MIN_PHASE							(0)
#define MIN_CONTRACTING_TIME				(0)
#define MIN_EXTENDING_TIME					(0)
#define MIN_STRENGTH						(1)

#define MAX_CONTRACTED_LENGTH				(0.5)	// m
#define MAX_EXTENDED_LENGTH					(0.5) 	// m
#define MAX_PHASE							(4)		// s
#define MAX_CONTRACTING_TIME				(2)		// s
#define MAX_EXTENDING_TIME					(2)		// s
#define MAX_STRENGTH						(100)	// (Kg)*m/s^2


class Muscle
{
private:
	double contracted_length;
	double extended_length;
	const double contracting_time;
	const double extending_time;
	const double phase;
	const double strength;
	double current_time;

	const unsigned int joint_1_idx;
	const unsigned int joint_2_idx;

	__host__ __device__ const Vector2 getLengthVector(Joint * joints) const;
	__host__ __device__ double getAppliedForceSize(Joint * joints) const;
	__host__ __device__ const Vector2 getAppliedForceDirection(Joint * joints) const;

public:
	__host__ Muscle(const unsigned int joint_1, const unsigned int joint_2);
	__host__ Muscle(
		const double contracted_length,
		const double extended_length,
		const double phase,
		const double contracting_time,
		const double extending_time,
		const double strength,
		const unsigned int joint_1_idx,
		const unsigned int joint_2_idx);
	__host__ Muscle mutate(double factor);

	__host__ __device__ unsigned int getJoint1Idx() const;
	__host__ __device__ unsigned int getJoint2Idx() const;
	__host__ __device__ void addMuscleForceToJoints(double time, Joint * joints);
	__host__ __device__ void applyCollisionIfExists(Joint * joints) const;

	__host__ void reset();
};


Muscle::Muscle(const unsigned int joint_1_idx, const unsigned int joint_2_idx) :
	contracted_length(rand_range(MIN_CONTRACTED_LENGTH, MAX_CONTRACTED_LENGTH)),
	extended_length(rand_range(MIN_EXTENDED_LENGTH, MAX_EXTENDED_LENGTH)),
	contracting_time(rand_range(MIN_CONTRACTING_TIME, MAX_CONTRACTING_TIME)),
	extending_time(rand_range(MIN_EXTENDING_TIME, MAX_EXTENDING_TIME)),
	phase(rand_range(MIN_PHASE, MAX_PHASE)),
	strength(rand_range(MIN_STRENGTH, MAX_STRENGTH)),
	joint_1_idx(joint_1_idx),
	joint_2_idx(joint_2_idx),
	current_time(0)
{
	// Swapping lengths if they are upside down.
	if (this->contracted_length > this->extended_length)
	{
		double temp = this->contracted_length;
		this->contracted_length = this->extended_length;
		this->extended_length = temp;
	}
}

Muscle::Muscle(
	const double contracted_length,
	const double extended_length,
	const double phase,
	const double contracting_time,
	const double extending_time,
	const double strength,
	const unsigned int joint_1_idx,
	const unsigned int joint_2_idx) :
	contracted_length(contracted_length),
	extended_length(extended_length),
	phase(phase),
	contracting_time(contracting_time),
	extending_time(extending_time),
	strength(strength),
	joint_1_idx(joint_1_idx),
	joint_2_idx(joint_2_idx),
	current_time(0)
{
	// Swapping lengths if they are upside down.
	if (this->contracted_length > this->extended_length)
	{
		double temp = this->contracted_length;
		this->contracted_length = this->extended_length;
		this->extended_length = temp;
	}
}

Muscle Muscle::mutate(double factor)
{
	// mutating only one of the values (50% chance of mutating nothing)
	unsigned int rand_val = rand() % 12;
	return Muscle(
		mutate_value(this->contracted_length, MIN_CONTRACTED_LENGTH, MAX_CONTRACTED_LENGTH, (rand_val == 0) * factor),
		mutate_value(this->extended_length, MIN_EXTENDED_LENGTH, MAX_EXTENDED_LENGTH, (rand_val == 1) * factor),
		mutate_value(this->phase, MIN_PHASE, MAX_PHASE, (rand_val == 2) * factor),
		mutate_value(this->contracting_time, MIN_CONTRACTING_TIME, MAX_CONTRACTING_TIME, (rand_val == 3) * factor),
		mutate_value(this->extending_time, MIN_EXTENDING_TIME, MAX_EXTENDING_TIME, (rand_val == 4) * factor),
		mutate_value(this->strength, MIN_STRENGTH, MAX_STRENGTH, (rand_val == 5) * factor),
		this->joint_1_idx,
		this->joint_2_idx
	);
}

// Returns the length vector from joint 1 to joint 2.
const Vector2 Muscle::getLengthVector(Joint * joints) const
{
	return joints[joint_2_idx].getLocation() - joints[joint_1_idx].getLocation();
}

double Muscle::getAppliedForceSize(Joint * joints) const
{
	double cur_length = getLengthVector(joints).getSize();
	double phase_time = fmod((phase + current_time), (contracting_time + extending_time));
	int current_state = phase_time < contracting_time ? CONTRACTING : EXTENDING;

	if (current_state == CONTRACTING &&
		cur_length > contracted_length)
	{
		// Contracting is positive because the direction of the force is internal.
		return strength * MIN(cur_length - contracted_length, 1);
	}
	else if (current_state == EXTENDING &&
		extended_length > cur_length)
	{
		// Extending is negative because the direction of the force is internal.
		return -strength * MIN(extended_length - cur_length, 1);
	}

	// If the muscle is totally contracted or extended, it applies no force.
	return 0;
}

const Vector2 Muscle::getAppliedForceDirection(Joint * joints) const
{
	return getLengthVector(joints).normalize();
}

unsigned int Muscle::getJoint1Idx() const
{
	return joint_1_idx;
}

unsigned int Muscle::getJoint2Idx() const
{
	return joint_2_idx;
}

void Muscle::addMuscleForceToJoints(double time, Joint * joints)
{
	// Setting the new time so we will know the muscle state.
	current_time += time;

	double force_size = getAppliedForceSize(joints);
	Vector2 force_direction = getAppliedForceDirection(joints);

	// The applied force direction is from 1 to 2, thus joint_2 is the negative one.
	joints[joint_1_idx].addForce(force_direction * force_size);
	joints[joint_2_idx].addForce(-(force_direction * force_size));
}


void Muscle::applyCollisionIfExists(Joint * joints) const
{
	Vector2 length_vector = getLengthVector(joints);
	double cur_length = length_vector.getSize();
	double joint_1_collision_speed = joints[joint_1_idx].getSpeed().getDirectionalSize(length_vector);
	double joint_2_collision_speed = joints[joint_2_idx].getSpeed().getDirectionalSize(length_vector);

	if ((cur_length <= contracted_length &&
		joint_2_collision_speed - joint_1_collision_speed < 0) ||
		(cur_length >= extended_length &&
			joint_2_collision_speed - joint_1_collision_speed > 0))
	{
		double joint_1_mass = joints[joint_1_idx].getMass();
		double joint_2_mass = joints[joint_2_idx].getMass();
		double new_speed = (joint_1_mass * joint_1_collision_speed + joint_2_mass * joint_2_collision_speed) /
			(joint_1_mass + joint_2_mass);

		joints[joint_1_idx].setSpeedInDirection(length_vector, new_speed);
		joints[joint_2_idx].setSpeedInDirection(length_vector, new_speed);
	}
}

void Muscle::reset()
{
	this->current_time = 0;
}
