#pragma once

#include "Vector2.cuh"

#define FLOOR				(0)
#define PIPE				(1)


#define GROUND				(0)
#define PIPE_HEIGHT			(0.15)


class Environment
{
	unsigned int kind;
public:
	__host__ __device__ Environment(unsigned int kind);

	__host__ __device__ const Vector2 getNormalForce(const Vector2 &location, const Vector2 & totalForce) const;
	__host__ __device__ void fixLocationAndSpeed(Vector2 &location, Vector2 &speed) const;
};

Environment::Environment(unsigned int kind):
	kind(kind)
{
}

const Vector2 Environment::getNormalForce(const Vector2 & location, const Vector2 & totalForce) const
{
	double ground_directed_force = 0;

	switch (kind)
	{
	case FLOOR:
		ground_directed_force = totalForce.getDirectionalSize(Vector2(0, -1));

		if (location[HEIGHT] <= GROUND + EPSILON && ground_directed_force > 0)
		{
			return Vector2(0, 1) * ground_directed_force;
		}

		return Vector2(0, 0);

	case PIPE:
		ground_directed_force = totalForce.getDirectionalSize(Vector2(0, -1));

		if (location[HEIGHT] <= GROUND + EPSILON && ground_directed_force > 0)
		{
			return Vector2(0, 1) * ground_directed_force;
		}
		else if (location[HEIGHT] >= PIPE_HEIGHT - EPSILON && ground_directed_force < 0)
		{
			return Vector2(0, -1) * ground_directed_force;
		}

		return Vector2(0, 0);

	default:
		return Vector2(0, 0);
	}
}

void Environment::fixLocationAndSpeed(Vector2 & location, Vector2 & speed) const
{
	Vector2 floor_normal(0, 1);
	Vector2 ceiling_normal(0, -1);

	switch (kind)
	{
	case FLOOR:
		if (location[HEIGHT] < GROUND + EPSILON)
		{
			location[HEIGHT] = GROUND;

			if (speed.getDirectionalSize(floor_normal) < 0)
			{
				speed.setDirectionalSize(floor_normal, 0);
			}
		}

		break;

	case PIPE:
		if (location[HEIGHT] < GROUND + EPSILON)
		{
			location[HEIGHT] = GROUND;

			if (speed.getDirectionalSize(floor_normal) < 0)
			{
				speed.setDirectionalSize(floor_normal, 0);
			}
		}
		if (location[HEIGHT] > PIPE_HEIGHT - EPSILON)
		{
			location[HEIGHT] = PIPE_HEIGHT;

			if (speed.getDirectionalSize(ceiling_normal) < 0)
			{
				speed.setDirectionalSize(ceiling_normal, 0);
			}
		}
		break;

	default:
		break;
	}
}
