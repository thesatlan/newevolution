#pragma once

#include "Environment.cuh"

#define FOLLOW_LOCATION (500)
#define MAGNIFIER		(200)

void draw_environment(unsigned int environment)
{
	int floor = 0;
	int ceiling = 0;

	switch (environment)
	{

	case FLOOR:
		floor = 300 - MAGNIFIER * GROUND;
		line(0, floor, 1000, floor);
		break;

	case PIPE:
		floor = 300 - MAGNIFIER * GROUND;
		ceiling = 300 - (10 + MAGNIFIER * PIPE_HEIGHT);
		line(0, floor, 1000, floor);
		line(0, ceiling, 1000, ceiling);
		break;

	default:
		break;
	}
}

int x_draw_loc(double x, double follow)
{
	return (int)(FOLLOW_LOCATION + MAGNIFIER * (x - follow));
}

int y_draw_loc(double y, bool best = false)
{
	if (best)
	{
		return 300 + (5 + (int)(MAGNIFIER * y));
	}
	else
	{
		return 300 - (5 + (int)(MAGNIFIER * y));
	}
}

void draw_distance(double distance, double follow)
{
	int draw_loc = x_draw_loc(distance, follow);

	line(draw_loc, 100, draw_loc, 300);

	char text[10];
	snprintf(text, 10, "%.1f", distance);
	outtextxy(draw_loc + 5, 100, text);
}

void print_telemetry(
	unsigned int gen,
	double time,
	double best_score,
	unsigned int best_gen,
	double current_best)
{
	char buff[50];
	snprintf(buff, 50, "Generation: %d", gen);
	outtextxy(10, 10, buff);
	snprintf(buff, 50, "Time:       %.1f", time);
	outtextxy(10, 25, buff);
	snprintf(buff, 50, "Best score: %.1f (%d)", best_score, best_gen);
	outtextxy(10, 40, buff);
	snprintf(buff, 50, "Current:    %.1f", current_best);
	outtextxy(10, 55, buff);

}

void draw_creature(Creature *creature, double follow, bool best = false)
{
	// Drawing joints
	for (unsigned int i = 0; i < creature->getJointsNum(); i++)
	{
		Vector2 joint_loc = creature->getJoints()[i].getLocation();
		circle(x_draw_loc(joint_loc[0], follow), y_draw_loc(joint_loc[1], best), 5);
	}

	// Drawing muscles
	for (unsigned int i = 0; i < creature->getMusclesNum(); i++)
	{
		Vector2 joint1_loc = creature->getJoints()[creature->getMuscles()[i].getJoint1Idx()].getLocation();
		Vector2 joint2_loc = creature->getJoints()[creature->getMuscles()[i].getJoint2Idx()].getLocation();

		line(
			x_draw_loc(joint1_loc[0], follow),
			y_draw_loc(joint1_loc[1], best),
			x_draw_loc(joint2_loc[0], follow),
			y_draw_loc(joint2_loc[1], best));
	}

	// Drawing the center of mass
	Vector2 center = creature->getCenterOfMass();
	circle(x_draw_loc(center[0], follow), y_draw_loc(center[1], best), 2);
}
