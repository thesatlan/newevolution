#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <algorithm>
#include <random>

#include "graphics.h"
#include "Vector2.cuh"
#include "Environment.cuh"
#include "Joint.cuh"
#include "Muscle.cuh"
#include "Creature.cuh"
#include "Drawings.h"

#define MAX(a, b) ((a) < (b) ? (b) : (a))

#define POPULATION (2048)
#define BLOCKS (32)

#define STEP_TIME		(0.005)
#define STEPS_BATCH		((int)(0.1 / STEP_TIME))
#define RUN_TIME		(30.001)

#define MUTATION_FACTOR	(0.1)

#define GPU				(true)
#define ENV				(FLOOR)

#define BEST_CREATURE	(POPULATION)

__global__ void move_creatures(const Environment * env, Creature** creatures, unsigned int steps)
{
	int i = blockIdx.x * blockDim.x + threadIdx.x;

	env->getNormalForce(Vector2(0, 0), Vector2(-1, -2));

	for (unsigned int step = 0; step < steps; step++)
	{
		creatures[i]->step(STEP_TIME, env);
	}
	creatures[i]->calculateCenterOfMass();
}

void move_creature(const Environment * env, Creature* creature, unsigned int steps)
{
	for (unsigned int step = 0; step < steps; step++)
	{
		creature->step(STEP_TIME, env);
	}
	creature->calculateCenterOfMass();
}

void reproduce(
	Creature ** creatures,
	unsigned int population_size,
	const double quarters_death_rates[4],
	const double quarters_reproduction_rates[4],
	double mutation_factor);




int main()
{
	srand((unsigned int)time(0));

	int gd = DETECT, gm;
	initgraph(&gd, &gm, "");

	Creature** creatures = NULL;
	cudaMallocManaged((void**)&creatures, (POPULATION + 1) * sizeof(Creature*));
	cudaMallocManaged((void**)&creatures[0], (POPULATION + 1) * sizeof(Creature));

	for (int i = 0; i < POPULATION + 1; i++)
	{
		creatures[i] = creatures[0] + i;
		// Constructing creatures on the allocated space.
		new (creatures[i]) Creature();
	}

	// Saving the best.
	double last_best = 0;
	unsigned int best_generation = 0;

	Environment * environment = NULL;
	cudaMallocManaged((void**)&environment, sizeof(Environment));
	new (environment) Environment(ENV);

	for (unsigned int generation = 1; true; generation++)
	{
		for (int i = 0; i < POPULATION; i++)
		{
			creatures[i]->reset();
		}
		creatures[BEST_CREATURE]->reset();

		for (double time = 0; time < RUN_TIME; time += STEPS_BATCH * STEP_TIME)
		{
			// Moving all of the creatures. You can move them by GPU (recommended) or CPU.
			if (GPU)
			{
				// Moving the best creature there was.
				move_creatures <<<1, 1>>> (environment, &creatures[BEST_CREATURE], STEPS_BATCH);

				// Moving all of the creatures.
				move_creatures<<<BLOCKS, POPULATION/BLOCKS>>>(environment, creatures, STEPS_BATCH);
				cudaError_t err = cudaDeviceSynchronize();
				if (err != cudaSuccess)
				{
					printf("cudaDeviceSynchronize failed!\n");
					goto cleanup;
				}
			}
			else
			{
				// Moving all the creatures including the best one.
				for (int i = 0; i < POPULATION + 1; i++)
				{
					move_creature(environment, creatures[i], STEPS_BATCH);
				}
			}

			// Killing creatures that have fallen.
			for (int i = 0; i < POPULATION; i++)
			{
				if (creatures[i]->getCenterOfMass()[1] == 0)
				{
					creatures[i]->~Creature();
					new (creatures[i]) Creature();
				}
			}

			// Moving the best creature there was.
			//move_creature(environment, best_creature, STEPS_BATCH);

			std::sort(creatures, creatures + POPULATION, [](const Creature* a, const Creature* b) -> bool { return *b < *a; });
			

			cleardevice();
			setcolor(WHITE);
			print_telemetry(
				generation, 
				time, 
				last_best,
				best_generation,
				creatures[0]->getCenterOfMass()[0]
			);

			double follow = creatures[0]->getCenterOfMass()[0];

			draw_environment(ENV);
			draw_distance((int)follow - 2, follow);
			draw_distance((int)follow - 1, follow);
			draw_distance((int)follow, follow);
			draw_distance((int)follow + 1, follow);
			draw_distance((int)follow + 2, follow);

			setcolor(CYAN);
			draw_creature(creatures[0], follow);
			setcolor(RED);
			draw_creature(creatures[1], follow);
			setcolor(GREEN);
			draw_creature(creatures[2], follow);
			setcolor(YELLOW);
			draw_creature(creatures[3], follow);
			setcolor(LIGHTGRAY);
			draw_creature(creatures[BEST_CREATURE], follow, true);
		}

		if (last_best < creatures[0]->score())
		{
			last_best = creatures[0]->score();
			best_generation = generation;
			creatures[BEST_CREATURE]->~Creature();
			new (creatures[BEST_CREATURE]) Creature(*creatures[0]);
		}

		double quarters_death_rates[4] = { 1, 0.8, 0.5, 0.1 };
		double quarters_reproduction_rates[4] = { 0.3, 0.3, 0.6, 1 };
		reproduce(
			creatures,
			POPULATION,
			quarters_death_rates,
			quarters_reproduction_rates,
			MUTATION_FACTOR);
	}
cleanup:
	readkey();

	environment->~Environment();
	cudaFree(environment);

	for (int i = 0; i < POPULATION + 1; i++)
	{
		creatures[i]->~Creature();
	}
	cudaFree(creatures);

	closegraph();
}












unsigned int increase_reproduction_idx(
	unsigned int reproduce_idx,
	unsigned int quarter_size,
	const double quarters_death_rates[4])
{
	reproduce_idx++;
	for (int i = 0; i < 4; i++)
	{
		// The checked zone is the place of the creatures that stayed alive.
		if (reproduce_idx >= quarter_size * (i + quarters_death_rates[i]) &&
			reproduce_idx < quarter_size * (i + 1))
		{
			reproduce_idx = quarter_size * (i + 1);
		}
	}

	return reproduce_idx;
}

void reproduce(
	Creature ** creatures,
	unsigned int population_size,
	const double quarters_death_rates[4],
	const double quarters_reproduction_rates[4],
	double mutation_factor)
{
	unsigned int quarter_size = population_size / 4;

	// Reversing the array to be from worst to best.
	std::reverse(creatures, creatures + population_size);

	// Shuffling each quarter.
	for (int i = 0; i < 4; i++)
	{
		// We dont want to shuffle the last spot because it is the BEST creature.
		int is_last_spot = i == 4 ? 1 : 0;

		std::random_shuffle(
			creatures + i * quarter_size,
			creatures + (i + 1) * quarter_size - is_last_spot);
	}

	// Notice that there must be a death rate to the first quarter.
	unsigned int next_reproduce_idx = 0;

	for (int quarter = 0; quarter < 4; quarter++)
	{
		for (unsigned int i = 0; i < quarter_size; i++)
		{
			if (quarter * quarter_size + i == next_reproduce_idx &&
				i < quarter_size * quarters_death_rates[quarter] &&
				i < quarter_size * quarters_reproduction_rates[quarter])
			{
				// In case we need to kill and reproduce the creature, 
				// we need to save the parent first.
				Creature current_creature = *creatures[quarter * quarter_size + i];
				creatures[quarter * quarter_size + i]->~Creature();
				new (creatures[next_reproduce_idx]) Creature(
					current_creature.mutate(mutation_factor)
				);

				next_reproduce_idx = increase_reproduction_idx(
					next_reproduce_idx,
					quarter_size,
					quarters_death_rates);
			}
			else
			{
				if (i < quarter_size * quarters_reproduction_rates[quarter])
				{
					// Reproducing the creature.
					new (creatures[next_reproduce_idx]) Creature(
						creatures[quarter * quarter_size + i]->mutate(mutation_factor)
					);

					next_reproduce_idx = increase_reproduction_idx(
						next_reproduce_idx,
						quarter_size,
						quarters_death_rates);
				}
				if (i < quarter_size * quarters_death_rates[quarter])
				{
					// Killing the creature.
					creatures[quarter * quarter_size + i]->~Creature();
				}
			}
		}
	}

	// Creating brand new creatures to fill the population.
	while (next_reproduce_idx < population_size)
	{
		new (creatures[next_reproduce_idx]) Creature();

		next_reproduce_idx = increase_reproduction_idx(
			next_reproduce_idx,
			quarter_size,
			quarters_death_rates);
	}
}



